export const SHOW_ADD_TO_CART_MODAL_WINDOW = "SHOW_ADD_TO_CART_MODAL_WINDOW";
export const SHOW_CONFIRM_DELETE_MODAL_WINDOW = "SHOW_CONFIRM_DELETE_MODAL_WINDOW";
export const CLOSE_MODAL_WINDOW = "CLOSE_MODAL_WINDOW";

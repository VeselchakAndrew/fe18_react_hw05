import {combineReducers} from "redux";
import "./itemsReducer"
import itemsReducer from "./itemsReducer";
import modalReducer from "./modalReducer";
import formReducer from "./formReducer";

const rootReducer =  combineReducers({
    items: itemsReducer,
    modals: modalReducer,
    forms: formReducer
});

export default rootReducer;


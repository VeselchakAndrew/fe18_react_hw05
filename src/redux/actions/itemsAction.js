import {
    GET_GOODS_FROM_JSON,
    IS_LOADING,
    ADD_GOODS_IN_CART,
    REMOVE_GOODS_FROM_CART,
    SET_GOODS_IN_FAVOURITE,
    REMOVE_CART_INFO_FROM_LOCALSTORAGE,
    IS_UPDATED
} from "../itemsActionTypes";


export const getGoods = () => async dispatch => {
    dispatch({
        type: IS_LOADING,
        payload: true
    })
    const goodsSource = "./baza.json"
    const receivedGoodsData = await fetch(goodsSource);
    const goodsData = await receivedGoodsData.json();
    dispatch({
        type: GET_GOODS_FROM_JSON,
        payload: goodsData
    })
}

export const getDataIDFromLS = (key) => {

    if (!localStorage.getItem(key)) {
        (localStorage.setItem(key, JSON.stringify([])))
    }

    const data = JSON.parse(localStorage.getItem(key))

    return ({
        type: `GET_GOODS_ID_IN_${key.toUpperCase()}`,
        payload: data
    })
}

export const matchData = (key, actionType) => async dispatch => {

    const goodsSource = "./baza.json"
    const receivedGoodsData = await fetch(goodsSource);
    const goodsData = await receivedGoodsData.json();
    const getIDs = await JSON.parse(localStorage.getItem(key));

    const data = await goodsData.filter(e => getIDs.some((item) => e.vendorCode === item));

    dispatch({
        type: `GET_GOODS_IN_${actionType}`,
        payload: data
    })
}

export const updateData = (bool) => {
    return {
        type: IS_UPDATED,
        payload: bool
    }
}

export const removeCartInfoFromLS = () => dispatch => {
    localStorage.setItem("Cart", JSON.stringify([]));

    dispatch({
        type: REMOVE_CART_INFO_FROM_LOCALSTORAGE
    })
}

export const addToCart = (goodsInCartID, itemIdToAdd) => dispatch => {

    const isIdExist = goodsInCartID.includes(itemIdToAdd);

    if (!isIdExist) {
        localStorage.setItem("Cart", JSON.stringify([...goodsInCartID, itemIdToAdd]))
        dispatch({
            type: ADD_GOODS_IN_CART,
            payload: [...goodsInCartID, itemIdToAdd]
        })
    }
}

export const removeFromCart = (goodsInCartID, itemIdToRemove) => dispatch => {
    const newData = goodsInCartID.filter(e => itemIdToRemove !== e);
    localStorage.setItem("Cart", JSON.stringify(newData))
    dispatch({
        type: REMOVE_GOODS_FROM_CART,
        payload: newData
    })
}

export const setFavorite = (goodsInFavouriteID, id) => dispatch => {

    const isIdExist = goodsInFavouriteID.includes(id);
    let newData = [];
    if (!isIdExist) {
        newData = [...goodsInFavouriteID, id];
        localStorage.setItem("Favourite", JSON.stringify(newData));

    } else {
        newData = goodsInFavouriteID.filter(e => e !== id);
        localStorage.setItem("Favourite", JSON.stringify(newData));
    }
    dispatch({
        type: SET_GOODS_IN_FAVOURITE,
        payload: newData
    })
}


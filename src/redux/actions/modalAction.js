import {
    SHOW_ADD_TO_CART_MODAL_WINDOW,
    SHOW_CONFIRM_DELETE_MODAL_WINDOW,
    CLOSE_MODAL_WINDOW
} from "../modalsActionTypes";

export const showAddToCartModalWindow = () => {
    return {
        type: SHOW_ADD_TO_CART_MODAL_WINDOW,
        payload: true
    }
}

export const showConfirmDeletionModalWindow = () => {
    return {
        type: SHOW_CONFIRM_DELETE_MODAL_WINDOW,
        payload: true

    }
}

export const closeModalWindow = () => {
    return {
        type: CLOSE_MODAL_WINDOW
    }
}
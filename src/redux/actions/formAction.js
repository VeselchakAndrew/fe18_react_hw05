import {SAVE_ORDER_INFO_FROM_FORM} from "../formActionTypes";

export const saveOrderData = (data) => dispatch => {
    dispatch({
        type: SAVE_ORDER_INFO_FROM_FORM,
        payload: data
    })
}
import './App.scss';
import React from "react";
import Header from "./Header/Header"
import Footer from "./Footer/Footer";
import Body from "./Body/Body";


const App = () => {

    return (
        <div className="app">
            <Header/>
            <Body/>
            <Footer/>
        </div>
    );
}

export default App;
